Source: tuf
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Philippe Coval <rzr@users.sf.net>
Section: python
Priority: optional
Build-Depends: dh-python,
               debhelper-compat (= 13),
               python3-all,
               python3-coverage,
               python3-cryptography,
               python3-dateutil,
               python3-nacl,
               python3-requests,
               python3-securesystemslib,
               tox
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/python-team/packages/tuf/-/tree/debian/master
Vcs-Git: https://salsa.debian.org/python-team/packages/tuf.git
Homepage: https://theupdateframework.com
Testsuite: autopkgtest-pkg-python

Package: python3-tuf
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Description: plug-and-play library for securing a software updater
 The Update Framework (TUF) helps developers to maintain the security of a
 software update system, even against attackers that compromise the repository
 or signing keys.
 .
 tuf is developed at the Secure Systems Lab of NYU.

Package: python-tuf-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Documentation and specification for TUF library and more
 TUF's documentation from source package, it contains specification,
 tutorial and more detailed documents, more can be found online.
